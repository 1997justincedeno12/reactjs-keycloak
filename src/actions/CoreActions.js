import Keycloak from 'keycloak-js';
import Config from './../config';

export const CoreActions = {
  authenticate(callback = () => null) {
    return (dispatch) => {
      CoreActions.keycloak = new Keycloak(Config.KEYCLOAK);
      CoreActions.keycloak.onTokenExpired = async () => {
        await CoreActions.keycloak.updateToken(40);
      };

      dispatch({ type: 'USER_AUTHENTICATE', payload: {} });
      CoreActions.keycloak.init({
        onLoad: 'login-required', promiseType: 'native'
      }).then(authenticated => {
        dispatch({
          type: 'USER_AUTHENTICATE_RESULT',
          payload: { authenticated }
        });
        if (authenticated) {
          return callback(null, authenticated);
        } else {
          return callback('Not authenticated');
        }
      }).catch(error => {
        console.error(error);
      });
    }
  },
  logout(callback = () => null) {
    return (dispatch) => {
      CoreActions.keycloak.logout();
      return callback(null);
    };
  },
}
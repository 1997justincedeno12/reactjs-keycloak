import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import { AppReducer } from './../reducer/AppReducer';

const makeStore = (reducer = {}) => {
  return createStore(
    reducer,
    applyMiddleware(thunk)
  );
};

export const AppStore = makeStore(AppReducer);
import React, { Component } from 'react';
import { Provider, connect } from 'react-redux';

import { CoreActions } from './actions';
import { AppStore } from './core';
import { WelcomeScreen } from './component/screens';

class AppComponent extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(CoreActions.authenticate((error, result) => {
      if (error) {
        console.error({ error });
      }
    }));
  }

  render() {
    return (
      <WelcomeScreen />
    );
  }
}

const mapStateToProps = (state, dispatch) => {
  return {
    state,
    dispatch
  };
}

const App = connect(mapStateToProps)(AppComponent);

function AppContainer() {
  return (
    <Provider store={AppStore}>
      <App />
    </Provider>
  );
}


export default AppContainer;

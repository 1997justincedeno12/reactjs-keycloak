import React, { Component } from 'react';
import { connect } from 'react-redux';
import welcome from './../../assets/images/welcome.svg';
import { CoreActions } from './../../actions';

export class WelcomeComponent extends Component {
  render() {
    const { dispatch } = this.props;
    return (
      <div style={{ width: '100vw', height: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <img src={welcome} style={{ height: 500 }} />
        <button onClick={CoreActions.logout(() => {
        })}>Logout</button>
      </div>
    );
  }
};

const maptStateToProps = (state, dispatch) => {
  return {
    state,
    dispatch
  };
}

export const WelcomeScreen = connect(maptStateToProps)(WelcomeComponent);
const Config = {
  KEYCLOAK: {
    url: 'http://localhost:8080/auth', // rename it with the current url used for the keycloak
    realm: 'keycloak-demo', // rename it with the current realm used
    clientId: 'demo', // change it into the actual clientId used
    onLoad: 'login-required'
  }
};

export default Config;